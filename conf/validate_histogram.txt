#core_hist_val
   filters = {"building_class": ["Commercial"],
              "floor_area": { "min": 10000, "max": 50000 }};
   group_by = ["floor_area"]            
--

#validate_histogram_framework
   #set_target_histogram;
   clear all;
   #core_hist_val;
--

#val:hist:1
   #validate_histogram_framework;
   filters = {"building_class": "Commercial" };
   #fire_query;
--

#val:hist:2
   #validate_histogram_framework;
   group_by = "floor_area";
   #fire_query;
--

#val:hist:3
   #validate_histogram_framework;
   group_by = ["floor_area", "state", "city", "site_eui"];
   #fire_query;
--

#bug:hist:1
   #validate_histogram_framework;
   filters = {"building_class": ["Commercial"],"state": {"values": ["AK"],"exclude": "True"}};
   group_by = ["fuel_eui"];
   #fire_query
--

#bug:hist:17:1
   #validate_histogram_framework;
   filters = {"building_class": ["Commercial"],"state": {"values": ["AK"],"exclude": "True"}};
   group_by = ["operating_hours"];
   #fire_query

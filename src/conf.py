import pdb
import copy

def strip(x):
    return x.strip()

def isCommandBlock(block):
    return block[0].startswith("#")

def splitCommandsIntoBlocks(cmds):
    blocks = []
    current_block = []

    while len(cmds):
        cmd = cmds.pop(0)
        if cmd == "--":
            blocks.append(current_block)
            current_block = []
        else:
            current_block.append(cmd)
    blocks.append(current_block)

    blocks = filter(lambda x: len(x), blocks)
    
    return blocks




def processCommandBlock(cmd_block):
    # Filter out comments
    cmd_block = filter(lambda x: not x.strip().startswith("@"), cmd_block)

    cmd_block = cmd_block[:]
    cmd_name = cmd_block.pop(0)
    cmd_contents = cmd_block
    cmd_contents = map(strip, cmd_contents)

    all_commands = []
    curr_cmd_components = []

    for cmd in cmd_contents:
        curr_cmd_components.append(cmd)

        if cmd.endswith(";"):
            all_commands.append( "".join(curr_cmd_components))
            curr_cmd_components = []
    else:
        all_commands.append( "".join(curr_cmd_components))

    all_commands = filter(lambda x: x.strip(), all_commands)
    cmds = map(cleanupStrings, all_commands)
    return cmd_name, cmds

import re
def cleanupStrings(str):
    str = re.sub("\s+", " ", str)
    str = re.sub(",\s*", ", ", str)
    str = re.sub("\s*:\s*", ": ", str)    
    str = re.sub("\s*=\s*", " = ", str)    

    str = str.strip()

    while str.endswith(";"):
        str = str[:-1].strip()
    str = str.strip()
    return str


def processVariablesOutOfCommands(commands, variables):
    inp_commands = copy.deepcopy(commands)

    varInCommandSet = lambda cmd: "$" in "".join(inp_commands[cmd])

    commands_with_variables = filter(lambda cmd: varInCommandSet(cmd), inp_commands.keys())

    for cmd_name in commands_with_variables:
        cmd = inp_commands[cmd_name]
        inp_commands[cmd_name] = removeVariablesFromCommand(cmd, variables)

    ret_commands = inp_commands

    return ret_commands

def removeVariablesFromCommand(command_list, variables):
    command_list = copy.deepcopy(command_list)

    for (ndx, cmd) in enumerate(command_list):
        while "$" in cmd:
            for var in variables:
                if var in cmd:
                    cmd = cmd.replace(var, variables[var])
            else:
                if "$" in cmd:
                    raise Exception("Variable could not be substituted in line {0}".format(cmd))
        command_list[ndx] = cmd
                    
    
    return command_list

def processMacrosOutOfCommands(commands, cmd):

    hasMacro = lambda x: "#" in "".join(x)

    cmds = commands[cmd]

    while hasMacro(cmds):
        new_cmds = []
        while cmds:
            tok = cmds.pop(0)
            if tok.startswith("#"):
                # is macro
                new_cmds.extend(commands[tok])
            else:
                new_cmds.append(tok)
        cmds = new_cmds
    
    return cmds


##################################################
##################################################
##################################################

def processCommandFile(fn):

    strip = lambda x: x.strip()
    raw_commands = open(fn, 'r').readlines()
    cmds = filter(lambda x:x, map(strip, raw_commands))

    blocks = splitCommandsIntoBlocks(cmds)

    variables = {}
    commands = {}

    variable_blocks = filter(lambda x: not isCommandBlock(x), blocks)
    command_blocks = filter(isCommandBlock, blocks)

    for var_block in variable_blocks:
        for var_def_line in var_block:
            var_cmps = var_def_line.split()
            var_name = var_cmps[0]
            var_def = " ".join(var_cmps[1:])
            variables[var_name] = var_def

    for cmd_block in command_blocks:
        cmd_name, cmd_list = processCommandBlock(cmd_block)
        commands[cmd_name] = cmd_list

    return variables, commands

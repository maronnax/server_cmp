import pdb
import json

def validateAnalyzePeers(test_json_str):
    # Parameters (as one payload JSON object):
    # Name	Type	Description
    # filters 
    # required	filter JSON object	filterset defining the BPD peer group
    # eui_type optional	string	Allowed EUI types: 'total_source_consumption', 'total_site_consumption', 'fuel_consumption', 'electric_consumption'. Defaults to 'total_source_consumption'
    # keep_outliers optional	boolean	true: keeps EUI outliers in the results; false: removes EUI outliers less than 1 kBtu/sq ft/year and outside 2% and 98% of the distribution
    # number_of_bins optional	integer	sets the number of bins in the resulting histogram within a valid range of 1 and 200. Note: auto_bin overrides the number_of_bins parameter
    # auto_bin optional	boolean	If true, the number of histogram bins will be automatically set based on the number of buildings in the result (25 bins if less than 100 buildings, 50 bins if more than 100 buildings). If false and number_of_bins is not passes in the request, then the number of bins is set to 50. Note: auto_bin overrides the number_of_bins parameter

    test_json = json.loads(test_json_str)
    keys = test_json.keys()
    
    if not "filters" in keys: return False
    if not validateFilters(test_json["filters"]): return False


    if "eui_type" in keys:
        assert(test_json["eui_type"] in ['total_source_consumption', 'total_site_consumption', 'fuel_consumption', 'electric_consumption'])

    if "keep_outliers" in keys:
        assert(test_json["keep_outliers"] in [True, False])

    if "number_of_bins" in keys:
        assert(isinstance(test_json["number_of_bins"], int))
        assert( 1 <= test_json["number_of_bins"] <= 200)
        
    if "auto_bin" in keys:
        assert( test_json["auto_bin"] in [True, False])
    
    return True
values_classification_type = ["Commercial", "Residential"]
values_facility_type = ["Agricultural","Commercial - Uncategorized","Convenience store","Convenience store with gas station","Data Center","Education - College or university","Education - Elementary or middle school","Education - High school","Education - Other classroom","Education - Preschool or daycare","Education - Uncategorized","Food Sales","Food Service  - Other","Food Service  - Restaurant or cafeteria","Food Service - Bakery","Food Service - Fast food","Food Service - Uncategorized","Grocery store or food market","Health Care - Inpatient","Health Care - Outpatient Clinic","Health Care - Outpatient Diagnostic","Health Care - Outpatient Uncategorized","Health Care - Uncategorized","Industrial","Laboratory","Lodging -  Hotel","Lodging - Dormitory or fraternity/sorority","Lodging - Motel or inn","Lodging - Other","Lodging - Uncategorized","Nursing Home","Office  - Administrative or Professional","Office - Bank or other financial","Office - Government","Office - Medical non diagnostic","Office - Mixed use","Office - Other","Office - Uncategorized","Other","Parking Garage","Public Assembly - Arena","Public Assembly - Drama theater","Public Assembly - Entertainment/culture","Public Assembly - Large Hall","Public Assembly - Library","Public Assembly - Movie Theater","Public Assembly - Recreation","Public Assembly - Social/meeting","Public Assembly - Uncategorized","Public Safety - Courthouse","Public Safety - Fire or police station","Public Safety - Uncategorized","Religious worship","Residential","Retail - Big Box (> 50K sf)","Retail - Enclosed mall","Retail - Other than mall","Retail - Small Box (< 50K sf)","Retail - Strip shopping mall","Retail - Uncategorized","Retail - Vehicle dealership/showroom","Service  - Industrial shop","Service -  Post office or postal center","Service -  Repair shop","Service - Art/Video/Photography Studio","Service - Dry-cleaning or Laundry","Service - Other service","Service - Uncategorized","Service - Vehicle service/repair shop","Transportation Terminal","Vacant","Warehouse - Distribution or Shipping center","Warehouse - Non-refrigerated","Warehouse - Refrigerated","Warehouse - Self-storage","Warehouse - Uncategorized","2-4 Unit Building","5+ Unit Building","Apartment Unit","Manufactured Home","Multifamily - Condominiums","Multifamily - Uncategorized","Other","Single Family - Attached","Single Family - Detached","Single Family - Uncategorized","Unknown"]
values_climate_zone = ["1A Very Hot - Humid (Miami-FL)","2A Hot - Humid (Houston-TX)","2B Hot - Dry (Phoenix-AZ)","3A Warm - Humid (Memphis-TN)","3B Warm - Dry (El Paso-TX)","3C Warm - Marine (San Francisco-CA)","4A Mixed - Humid (Baltimore-MD)","4B Mixed - Dry (Albuquerque-NM)","4C Mixed - Marine (Salem-OR)","5A Cool - Humid (Chicago-IL)","5B Cool - Dry (Boise-ID)","6A Cold - Humid (Burlington-VT)","6B Cold -Dry (Helena-MT)","7 Very Cold (Duluth-MN)","8 Subarctic (Fairbanks-AK)"]
values_state = ["AL","AK","AZ","AR","CA","CO","CT","DE","DC","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY","PR"]
values_lighting = ["Compact Fluorescent","Fluorescent - T12","Fluorescent - T5","Fluorescent - T8","Fluorescent - Uncategorized","Halogen","High intensity discharge (HID)","Incandescent","LED","Mercury Vapor","Metal Halide","Other Or Combination","Sodium - High Pressure","Sodium - Low Pressure","Unknown"]
values_heating = ["Boiler - Hot Water","Boiler - Steam","Boiler - Uncategorized","Central Heating","District Hot Water","District Steam","Furnace","Heat Pump - Air Source","Heat Pump - Ground Source","Heat Pump - Uncategorized","Heat Pump - Water Loop","No Heating","Other Or Combination","PTAC","Perimeter Baseboard","Radiator","Resistance Heating","Unknown"]
values_cooling = ["Central Air Conditioning","Chiller - Absorption","Chiller - Engine Driven","Chiller - Turbine Driven","Chiller - Uncategorized","Condenser","Cooling Tower - Closed","Cooling Tower - Open","Cooling Tower - Uncategorized","District Chilled Water","Evaporative Cooler","Heat Pump - Air Source","Heat Pump - Ground Source","Heat Pump - Uncategorized","Heat Pump - Water Loop","No cooling","Other Or Combination","PTAC","Packaged Direct Expansion","Split AC System","Unknown"]
values_window_glass_type = ["Clear","High Performance","Low-e","Other Or Combination","Reflective","Tinted","Unknown"]
values_window_glass_layers = ["Double-pane","Multi-layered","Other Or Combination","Single-pane","Triple-pane","Unknown"]
values_air_flow_control = ["Constant Volume","Unknown","Variable Volume"]
values_roof_ceiling = ["Asphalt/fiberglass/other shingles","Built-up","Concrete","Cool Roof","Metal surfacing","Other Or Combination","Plastic/rubber/synthetic sheeting","Slate or tile shingles","Unknown","Wood shingles/shakes/other wood"]

def validateFilters(filters):
    k = filters.keys()
    if "classification_type" in k:
        val = filters["classification_type"]
        mustValidateAsList(val, values_classification_type)

    if "facility_type" in k:
        val = filters["facility_type"]
        mustValidateAsList(val, values_facility_type)

    if "floor_area" in k:
        val = filters["floor_area"]
        mustValidateAsRange(val, min=0, max = float('inf'))
    
    if "year_built" in k:
        val = filters["year_built"]
        mustValidateAsRange(val, min = 0, max = 10000)
    
    if "hours_occupied" in k:
        val = filters["hours_occupied"]
        mustValidateAsRange(val, 0, 168)
    
    if "number_of_people" in k:
        val = filters["number_of_people"]
        mustValidateAsRange(val, 0, float('inf'))
    
    if "climate_zone" in k:
        val = filters["climate_zone"]
        mustValidateAsList(val, values_climate_zone)
    
    if "state" in k:
        val = filters["state"]
        mustValidateAsList(val, values_state)
    
    if "zip_code" in k:
        val = filters["zip_code"]
        mustValidateAsList(val, False) # False means all are legal
    
    if "lighting" in k:
        val = filters["lighting"]
        mustValidateAsList(val, values_lighting)
    
    if "heating" in k:
        val = filters["heating"]
        mustValidateAsList(val, values_heating)
    
    if "cooling" in k:
        val = filters["cooling"]
        mustValidateAsList(val, values_cooling)
    
    if "window_glass_type" in k:
        val = filters["window_glass_type"]
        mustValidateAsList(val, values_window_glass_type)
    
    if "window_glass_layers" in k:
        val = filters["window_glass_layers"]
        mustValidateAsList(val, values_window_glass_layers)
    
    if "air_flow_control" in k:
        val = filters["flow_control"]
        mustValidateAsList(val, values_air_flow_control)
    
    if "wall_insulation_r_value" in k:
        val = filters["wall_insulation_r_value"]
        mustValidateAsRange(val, 0, 80)
    
    if "roof_ceiling" in k:
        val = filters["roof_ceiling"]
        mustValidateAsList(val, values_roof_ceiling)
    return

class ValidationError(Exception): pass

def rve(str):
    pdb.set_trace()
    raise ValidationError(str)

def mustValidateAsRange(val, min, max):
    if not isinstance(val, dict): rve("I expect {0} to be a dict but it isn't.".format(val))
    if not "min" in val: rve("Range {0} is missing min keyword".format(val))
    if not "max" in val: rve("Range {0} is missing max keyword".format(val))

    if not isinstance(val["min"], int): rve("{0} should be an integer but isn't".format(val[min]))
    if not isinstance(val["max"], int): rve("{0} should be an integer but isn't".format(val[max]))

    return True

def mustValidateAsList(val, list_of_val = False):

    def vl(val, list_of_val):
        for x in val:
            if not isinstance(x,basestring): rve("Value {0} should be a string but isn't.".format(x))

        if list_of_val:
            for x in val:
                if not x in list_of_val: rve("Value {0} should be in {1} but isn't.".format(x, list_of_val))
        return True

    if isinstance(val, list):
        vl(val, list_of_val)
        
    elif ifinstance(val, dict):
        if not "exclude" in val: rve("I expect value {0} to have an 'exclude' element but it doesn't".format(val))
        if not val["exclude"] in [True, False]: rve("Value {0} should be boolean but isn't.".format(val["exclude"]))
        if not "choices" in val: rve("I expect value {0} to have a 'choices' element but it doesn't".format(val))
        vl(val["choices"], list_of_val)

    return True

FILTER_LIST = {}

classification_type = ["Residential", "Commercial"]
facility_type = ["Agricultural", "Commercial - Uncategorized", "Convenience store",
                 "Convenience store with gas station", "Data Center",
                 "Education - College or university", "Education - Elementary or middle school",
                 "Education - High school", "Education - Other classroom",
                 "Education - Preschool or daycare", "Education - Uncategorized",
                 "Food Sales", "Food Service  - Other", "Food Service  - Restaurant or cafeteria",
                 "Food Service - Bakery", "Food Service - Fast food", "Food Service - Uncategorized",
                 "Grocery store or food market", "Health Care - Inpatient", "Health Care - Outpatient Clinic",
                 "Health Care - Outpatient Diagnostic", "Health Care - Outpatient Uncategorized",
                 "Health Care - Uncategorized", "Industrial", "Laboratory", "Lodging -  Hotel",
                 "Lodging - Dormitory or fraternity/sorority", "Lodging - Motel or inn",
                 "Lodging - Other", "Lodging - Uncategorized", "Nursing Home",
                 "Office  - Administrative or Professional", "Office - Bank or other financial",
                 "Office - Government", "Office - Medical non diagnostic", "Office - Mixed use",
                 "Office - Other", "Office - Uncategorized", "Other", "Parking Garage",
                 "Public Assembly - Arena", "Public Assembly - Drama theater",
                 "Public Assembly - Entertainment/culture", "Public Assembly - Large Hall",
                 "Public Assembly - Library", "Public Assembly - Movie Theater",
                 "Public Assembly - Recreation", "Public Assembly - Social/meeting",
                 "Public Assembly - Uncategorized", "Public Safety - Courthouse",
                 "Public Safety - Fire or police station", "Public Safety - Uncategorized",
                 "Religious worship", "Residential", "Retail - Big Box (> 50K sf)",
                 "Retail - Enclosed mall", "Retail - Other than mall", "Retail - Small Box (< 50K sf)",
                 "Retail - Strip shopping mall", "Retail - Uncategorized", "Retail - Vehicle dealership/showroom",
                 "Service  - Industrial shop", "Service -  Post office or postal center", "Service -  Repair shop",
                 "Service - Art/Video/Photography Studio", "Service - Dry-cleaning or Laundry",
                 "Service - Other service", "Service - Uncategorized", "Service - Vehicle service/repair shop",
                 "Transportation Terminal", "Vacant", "Warehouse - Distribution or Shipping center",
                 "Warehouse - Non-refrigerated", "Warehouse - Refrigerated", "Warehouse - Self-storage",
                 "Warehouse - Uncategorized", "2-4 Unit Building", "5+ Unit Building", "Apartment Unit",
                 "Manufactured Home", "Multifamily - Condominiums", "Multifamily - Uncategorized", "Other",
                 "Single Family - Attached", "Single Family - Detached", "Single Family - Uncategorized"]


climate_zone = ["1A Very Hot - Humid (Miami-FL)", "2A Hot - Humid (Houston-TX)",
                 "2B Hot - Dry (Phoenix-AZ)", "3A Warm - Humid (Memphis-TN)",
                 "3B Warm - Dry (El Paso-TX)", "3C Warm - Marine (San Francisco-CA)",
                 "4A Mixed - Humid (Baltimore-MD)", "4B Mixed - Dry (Albuquerque-NM)",
                 "4C Mixed - Marine (Salem-OR)", "5A Cool - Humid (Chicago-IL)",
                 "5B Cool - Dry (Boise-ID)", "6A Cold - Humid (Burlington-VT)",
                 "6B Cold -Dry (Helena-MT)", "7 Very Cold (Duluth-MN)",
                 "8 Subarctic (Fairbanks-AK)"]


state = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "HI",
         "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN",
         "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH",
         "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA",
         "WV", "WI", "WY", "PR"]

safe_states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "HI",
               "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN",
               "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NC", "ND", "OH",
               "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA",
               "WV", "WI", "WY"]

lighting = ["Compact Fluorescent", "Fluorescent - T12", "Fluorescent - T5", "Fluorescent - T8", "Fluorescent - Uncategorized", "Halogen", "High intensity discharge (HID)", "Incandescent", "LED", "Mercury Vapor", "Metal Halide", "Other Or Combination", "Sodium - High Pressure", "Sodium - Low Pressure"]
heating = ["Boiler - Hot Water", "Boiler - Steam", "Boiler - Uncategorized", "Central Heating", "District Hot Water", "District Steam", "Furnace", "Heat Pump - Air Source", "Heat Pump - Ground Source", "Heat Pump - Uncategorized", "Heat Pump - Water Loop", "No Heating", "Other Or Combination", "PTAC", "Perimeter Baseboard", "Radiator", "Resistance Heating"]
cooling = ["Central Air Conditioning", "Chiller - Absorption", "Chiller - Engine Driven", "Chiller - Turbine Driven", "Chiller - Uncategorized", "Condenser", "Cooling Tower - Closed", "Cooling Tower - Open", "Cooling Tower - Uncategorized", "District Chilled Water", "Evaporative Cooler", "Heat Pump - Air Source", "Heat Pump - Ground Source", "Heat Pump - Uncategorized", "Heat Pump - Water Loop", "No cooling", "Other Or Combination", "PTAC", "Packaged Direct Expansion", "Split AC System"]
window_glass_type = ["Clear", "High Performance", "Low-e", "Other Or Combination", "Reflective", "Tinted"]
window_layers = ["Double-pane", "Multi-layered", "Other Or Combination", "Single-pane", "Triple-pane"]
air_flow_control = ["Constant Volume", "Variable Volume"]
roof_ceiling = ["Asphalt/fiberglass/other shingles", "Built-up", "Concrete", "Cool Roof", "Metal surfacing", "Other Or Combination", "Plastic/rubber/synthetic sheeting", "Slate or tile shingles", "Wood shingles/shakes/other wood"]

# FILTER COMPONENTS =
# list_filter_components = [classification_type, facility_type, climate_zone, state, lighting, heating, cooling, window_glass_type, air_flow_control, roof_ceiling, zip_code]
# range_filter_components = [floor_area, year_built, hours_occupied, number_people, wall_insulation_r_value]
# special_filter_components = [zip_code]

def makeZipCodeList():
    return map(lambda x: "{0:05d}".format(x), range(1, 100000))

def createRandomListJsonCreator(prob_occurance, cond_prob_not, options_list, **kwds):
    # Def in some parameters about a function that will randomly
    # create a json element

    if not options_list :
        msg = "Must have non-zero length options list to make a random list json creator"
        raise Exception(msg)

    def inner():

        if random.random() < prob_occurance:
            return False

        cond_not = (random.random() < cond_prob_not)

        inner_opt_list = copy.deepcopy(options_list)

        max_res_list = min(kwds["max_length"], len(options_list)) if "max_length" in kwds else len(options_list)

        num_elements = random.randint(1, max_res_list)
        rand_list = []

        while num_elements:
            num_elements -= 1
            elmt = random.choice(inner_opt_list)
            inner_opt_list.pop(inner_opt_list.index(elmt))            
            rand_list.append(elmt)

        # if cond_not:
        #     return { "exclude": True,
        #              "choices": rand_list }
        # else:
        #     if random.random() < .05:
        #         return rand_list
        #     else:
        #         return { "exclude": False,
        #                  "choices": rand_list }
        return rand_list

    return inner

def createRandomRangeJsonCreator(prob_occurance, min_possible_val, max_possible_val, **kwds):
    # Def in some parameters about a function that will randomly
    # create a json element

    def inner():
        if random.random() < prob_occurance:
            return False

        rand1 = (max_possible_val - min_possible_val) * random.random() + min_possible_val
        rand2 = (max_possible_val - min_possible_val) * random.random() + min_possible_val
        
        lit_rand = int(max(min_possible_val, min(rand1, rand2)))
        big_rand = int(min(max_possible_val, int(max(rand1, rand2))))

        ret = {"min": lit_rand,
               "max": big_rand}
        return ret

    return inner

makeRandom_classification_type = createRandomListJsonCreator(.5, .25, classification_type, max_length = 1)
makeRandom_facility_type = createRandomListJsonCreator(.5, .25, facility_type, max_length = 10)
makeRandom_climate_zone = createRandomListJsonCreator(.5, .25, climate_zone, max_length = 4)

# This one is slightly odd because the BPD implementation is giving wacky results for NY and others.
makeRandom_state = createRandomListJsonCreator(1.0, -1.0, safe_states, max_length = 40) 
makeRandom_lighting = createRandomListJsonCreator(.05, .25, lighting, max_length = 10)
makeRandom_heating = createRandomListJsonCreator(.05, .25, heating, max_length = 10)
makeRandom_cooling = createRandomListJsonCreator(.05, .25, cooling, max_length = 10)
makeRandom_window_glass_type = createRandomListJsonCreator(.05, .25, window_glass_type, max_length = 4)
makeRandom_air_flow_control = createRandomListJsonCreator(.05, .25, air_flow_control, max_length = 2)
makeRandom_roof_ceiling = createRandomListJsonCreator(.05, .25, roof_ceiling, max_length = 7)
makeRandom_zip_code = createRandomListJsonCreator(.05, .25, makeZipCodeList(), max_length = 25)

makeRandom_floor_area = createRandomRangeJsonCreator(.01, 0, 10000)
makeRandom_year_built = createRandomRangeJsonCreator(.0, 1900, 2015)
makeRandom_hours_occupied = createRandomRangeJsonCreator(0, 0, 24)
makeRandom_number_people = createRandomRangeJsonCreator(0, 0, 100000)
makeRandom_wall_insulation_r_value = createRandomRangeJsonCreator(0, 0, 80)

def makeRandomFilter():
    obj = {}

    obj["classification_type"] = makeRandom_classification_type()
    obj["facility_type"] = makeRandom_facility_type()
    obj["climate_zone"] = makeRandom_climate_zone()
    obj["state"] = False #makeRandom_state()
    obj["lighting"] = False #makeRandom_lighting()
    obj["heating"] = False #makeRandom_heating()
    obj["cooling"] = False #makeRandom_cooling()
    obj["window_glass_type"] = False #makeRandom_window_glass_type()
    obj["air_flow_control"] = False #makeRandom_air_flow_control()
    obj["roof_ceiling"] = False #makeRandom_roof_ceiling()

    obj["floor_area"] = False #makeRandom_floor_area()
    obj["year_built"] = False #makeRandom_year_built()
    obj["hours_occupied"] = False #makeRandom_hours_occupied()
    obj["number_people"] = False   
    obj["wall_insulation_r_value"] = False # makeRandom_wall_insulation_r_value()

    obj["zip_code"] = False # makeRandom_zip_code()
    
    return {x: obj[x] for x in obj if obj[x] }

def makeRandomAnalyzeCountsPerState():
    req = { "filters": makeRandomFilter() }
    return req
    



#! /usr/bin/env python

__author__ = "Nathan Addy <nathan.addy@gmail.com>"

'''This tool provides a mechanism for sending requests to a server, or
alternatively, sending requests to two servers and comparing them.  It
is intended primarily as a tool for testing the new pizzazz
implementation of the BPD against the original implementation at bpd.lbl.gov.
'''

import sys
import pprint
import copy
import requests
import time
import json
import os
import pdb

import src.tools as tools
import src.conf as conf
input = raw_input # Python 3 compatibility.

# This sets requests to ignore unsigned certs. Temporarily using this
# because the pizzazz server is using self-signed certificates.  
VERIFY_SSL_CERT=False 

class InputException(Exception): pass

# This simple tool is built around these objects, which are kind of
# like a jumbo model (models the data to be sent to the server), the
# controller (sends the data to the controller), and the view (manages
# user interface).

class RequestResponseState(object):
    ''' A RequestResponseState is an object that manages a json request,
    a target, and responses that can come back.'''

    def __init__(self, **kwds):
        self.target = "http://NONE.COM"
        
        self.headers = {'Content-Type': 'application/json'}
        self.request = {}

        self.cache_size = kwds["cache_size"] if "cache_size" in kwds else 500

        self.request_history = []
        self.response_historys = []

        return 

    def transmit(self, post):
        self.request_history.append((self.target, copy.deepcopy(self.request), copy.deepcopy(self.headers)))
        payload = json.dumps(self.request)

        if post:
            response = requests.post(self.target, data = payload, headers = self.headers, verify=VERIFY_SSL_CERT)
        else:
            response = requests.get(self.target, data = payload, headers = self.headers, verify=VERIFY_SSL_CERT)
            
        self.response_historys.append(response)

        if not len(self.request_history) == len(self.response_historys):
            self.request_history = []
            self.response_historys = []

        while len(self.request_history) > self.cache_size:
            self.request_history.pop(0)
            self.response_historys.pop(0)

        return response

class ReqResponseStateWithPassword(RequestResponseState):
    '''Adds password features to the MVC object'''
    
    def __init__(self, **kwds):
        super(ReqResponseStateWithPassword, self).__init__(**kwds)
        self.username = "NONE"
        self.password = "NONE"
        return

    def getAuthorizationHeader(self):
        return ('Authorization',
                'ApiKey %s:%s' % (self.username, self.password))

    def updateHeadersWithAuthorization(self):
        key, value = self.getAuthorizationHeader()
        self.headers[key] = value
        return 

    def transmit(self, post):
        self.updateHeadersWithAuthorization()
        return super(ReqResponseStateWithPassword, self).transmit(post)

class InteractiveState(ReqResponseStateWithPassword):
    '''This is the fully interactive MVC class.  Lots of app features
    but fairly unwieldy.
    '''
    
    @staticmethod
    def _parseVariableId(var):
        return map(lambda x: x.strip(), var.split("::"))

    def getConfFiles(self):
        conf_dir = os.path.abspath(os.path.join( os.path.split(__file__)[0], "conf"))

        cmd_file_names = filter(lambda x: x.endswith("txt") and "#" not in x and not x.startswith("."), os.listdir(conf_dir))
        print "found {0}".format(cmd_file_names)
        return map(lambda x: os.path.join(conf_dir, x), cmd_file_names)

    def __init__(self, cache_size = 10):

        super(InteractiveState, self).__init__()

        
        self.conf_files = self.getConfFiles()
        self.variables = {}
        self.macros = {}

        self.base_website = ""
        self.api_target = "" # "/api/v1/analyze/counts-per-state/"

        self.initialize()
        return

    def clearState(self):
        print("Clearing data to {}.")
        self.request = {}

        print("Resetting username and password.")
        self.username = "NONE"
        self.password = "NONE"
        
        return 

    def getTargetWithBaseUrlAndFunc(self):
        return self.base_website + self.api_target

    def updateTargetWithBaseUrlAndFunc(self):
        self.target = self.getTargetWithBaseUrlAndFunc()
        return

    def reloadConfigurationFiles(self):
        self.conf_files = self.getConfFiles()
        conf_files = self.conf_files[:]
        variables = {}
        macros = {}
        try:
            while conf_files:
                fn = conf_files.pop()
                loc_var, loc_mac = conf.processCommandFile(fn)
                for (var, val) in loc_var.items():
                    variables[var] = val
                for (mac, commands) in loc_mac.items():
                    macros[mac] = commands
            else:
                macros =  conf.processVariablesOutOfCommands(macros, variables)

                # process command sub out of commands

                for cmd in macros:
                    macros[cmd] = conf.processMacrosOutOfCommands(macros, cmd)
        except Exception, xcpt:
            print "Got exception: {0}".format(xcpt)
            print "Continuing with a no op"
            return [], []
        
        self.variables = variables
        self.macros = macros
        return variables, macros

    def initialize(self):
        self.reloadConfigurationFiles()
        self.updateTargetWithBaseUrlAndFunc()
        self.updateHeadersWithAuthorization()
        self.request = {}
        return

    def processInput(self, input):
        '''
        '''
        inp = input.strip()

        try:
            self.parseAndUpdate(inp)

        except InputException, xcpt:
            print("Got InputException: {0}".format(xcpt))

        return     

    def transmit(self, post):
        pp = pprint.PrettyPrinter(indent=2)

        self.target = self.getTargetWithBaseUrlAndFunc() # Get the correct info

        req_type_str = "POST" if post else "GET"
        print("{1} to target {0}.".format(self.target, req_type_str))

        print("url: {0}".format(self.target))
        print("headers: {0}".format(self.headers))                
        print("payload: {0}".format(self.request))

        response = super(InteractiveState, self).transmit(post)
        print("Response: {0}".format(response.status_code))

        if not response.status_code == 200:
            print("Bad response.")
            return

        res = response.json()
        print("Got Response:")
        pp.pprint(res)
        print("-" * 75)
        return res

    def executeListOfCommands(self, cmd_list):
        for cmd in cmd_list:
            print "{0}".format(cmd)
            self.parseAndUpdate(cmd)
        return

    def parseAndUpdate(self, input):
        input = input.strip()
        if not input: return

        if input.startswith("#"):
            if not input in self.macros:
                print "Macro {0} does not exist".format(input)
            else:
                # print("Running macro {0}".format(input))
                self.executeListOfCommands(self.macros[input])
            return 

        if input.lower().startswith("set url"):
            self.base_website = input[len("set url"):].strip()
            return 

        if input.lower().startswith("set username"):
            self.username = input[len("set username"):].strip()
            self.updateHeadersWithAuthorization()            
            return
            
        if input.lower().startswith("set password"):
            self.password = input[len("set password"):].strip()
            self.updateHeadersWithAuthorization()
            return

        if input.strip() == "ls":
            print("Target: {0}".format(self.target))
            print("U/P: {0}/{1}".format(self.username, self.password))

            print("Data:")
            pp = pprint.PrettyPrinter(indent=2)            
            pp.pprint(self.request)
            return

        if input.startswith("clear"):
            if input == "clear all":
                current_target = self.target
                self.initialize()
                self.target = current_target

                print("Reset state. Target remains {0}".format(self.target))
                return
            else:

                def re(msg = ""):
                    raise InputException("Got bad clear statement. {0} '{1}'".format(msg, input))

                cmp_array = map(lambda x: x.strip(), input.split())
                if not len(cmp_array) == 2: re("Format should be 'clear SOME_VAR'")

                try:
                    var = self._parseVariableId(cmp_array[1])
                except Exception, xcpt:
                    re("Couldn't parse a variable from {0}.".format(cmp_array[1]))

                try:
                    self.clear(var)
                except KeyError:
                    pass
                
                return 

            print("Got unimplemented clear input '{0}'".format(input))
            
        if input.startswith("set target"):

            target = input[len("set target"):].strip()

            print("setting target to {0}".format(target))
            self.api_target = target
            self.target = self.base_website + self.api_target
            return

        if input.lower().startswith("ls macros"):
            mc = self.macros.keys()
            mc.sort()
            for x in mc:
                print x
            return

        if input.lower().startswith("reload conf"):
            print "Reloading configuration"
            self.reloadConfigurationFiles()
            return
        
        if input.endswith("!") or input.endswith("!P") or input.endswith("!G"):
            fire_to_server = True
            post_to_server = not input.endswith("!G")

            if input.endswith("!"):
                input = input[:-1].strip()
            else:
                input = input[:-2].strip()
            if "!" in input: raise InputException("Couldn't parse bad line '{0}'.".format(input.strip()))
            print("Sending message to server.")
            self.transmit(post_to_server)
            return 
        try:
            self._parseEqualsStatement(input)
        except Exception, xcpt:
            print("Parsing '{0}' failed with exception: {1} (expected equals statement)".format(input, xcpt))
        return

    def _parseEqualsStatement(self, input):

        if not input.count("=") == 1:
            raise InputException("Bad input: {0}".format(input))


        variable, equation = map(lambda x: x.strip(), input.split("="))

        var = self._parseVariableId(variable)
        result = eval(equation)

        self.set(var, result)
        return


    def clear(self, variable):
        var_to_pop = variable.pop()

        next = self.request
        for layer in variable:
            next = next[layer]
        else:
            next.pop(var_to_pop)
        return 

    def set(self, variable, result):
        last_dict = False
        curr_dict = self.request

        for var in variable:
            
            if var not in curr_dict:
                curr_dict[var] = {}
            last_dict = curr_dict                 
            curr_dict = curr_dict[var]
        else:
            last_dict[var] = result
        return

    def set(self, variable, result):
        last_dict = False
        curr_dict = self.request

        for var in variable:
            
            if var not in curr_dict:
                curr_dict[var] = {}
            last_dict = curr_dict                 
            curr_dict = curr_dict[var]
        else:
            last_dict[var] = result
        return     

def main():
    pp = pprint.PrettyPrinter(indent=2)
    state = InteractiveState()


    while True:
        inp = input("> ")
        if inp.lower().strip() == "quit":
            break
        else:
            try:
                state.processInput(inp)
            except requests.exceptions.ConnectionError, xcpt:
                print("Connection error.  Continuing.")
            except requests.exceptions.MissingSchema, xcpt:
                print "Missing schema error.  Have you set a url?"


    print("Quitting.")
    return

if __name__ == '__main__':
    main()
